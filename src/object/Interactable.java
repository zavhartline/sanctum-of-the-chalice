package object;

public interface Interactable {
	public abstract boolean interact(GameObject target);

}
